import { configureStore } from "@reduxjs/toolkit";
import productsReducer from "./productsSlice";
import modalSlice from '../store/modalSlice';

const store = configureStore({
  reducer: {
    products: productsReducer,
    modal: modalSlice,
  },
});

export default store;
