import { createSlice } from "@reduxjs/toolkit";

const modalSlice = createSlice({
  name: "modal",
  initialState: {
    modalType: "None",
  },
  reducers: {
    setModalType(state, action) {
      state.modalType = action.payload;
    },
  },
});

export const { setModalType } = modalSlice.actions;
export default modalSlice.reducer;
