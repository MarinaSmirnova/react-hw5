import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import Button from "../components/Button";
import Modal from "../components/Modal";
import CartItem from "../components/CartItem";
import { setModalType } from "../store/modalSlice";
import OrderForm from "../components/OrderForm";
import { NavLink } from "react-router-dom";

const Cart = () => {
  const [itemsInCart, setItemsInCart] = useState([]);
  const [activeProduct, setActiveProduct] = useState("");
  const [emptyCartMsg, setEmptyCartMsg] = useState("Cart is empty");

  const dispatch = useDispatch();
  const modalType = useSelector((state) => state.modal.modalType);

  const checkActiveProduct = (product) => {
    setActiveProduct(product);
  };

  const submitCart = () => {
    setEmptyCartMsg("Thank you for your order!");
    localStorage.setItem("productsInCart", JSON.stringify([]));
    setItemsInCart([]);
  };

  const openDeleteModal = (product) => {
    dispatch(setModalType("Delete"));
    checkActiveProduct(product);
  };

  const getProductsFromCartArray = () => {
    return JSON.parse(localStorage.getItem("productsInCart"));
  };

  const removeProductFromCart = (product) => {
    const productCartArray = getProductsFromCartArray();

    const newProductCartArray = productCartArray.filter(
      (productFromArray) => productFromArray.key !== product.key
    );

    localStorage.setItem("productsInCart", JSON.stringify(newProductCartArray));

    setItemsInCart([...newProductCartArray]);

    dispatch(setModalType("None"));
    setActiveProduct("");
  };

  const calculateTotal = () => {
    const productCartArray = getProductsFromCartArray();

    let totalSum = 0;

    if (productCartArray) {
      productCartArray.forEach((product) => {
        totalSum = totalSum + product.quantity * product.price;
      });
    }

    return totalSum;
  };

  useEffect(() => {
    setItemsInCart(() => [...getProductsFromCartArray()]);
  }, []);

  return (
    <>
      {itemsInCart.length !== 0 ? (
        <div className="cart__layout">
          <div className="cart__form-container">
            <OrderForm onSubmitFunc={submitCart} />
          </div>
          <div className="cart__products-container">
            <ul className="cart-list">
              {itemsInCart.map((product) => (
                <CartItem
                  key={product.id}
                  cardData={product}
                  buttons={
                    <>
                      <Button
                        clickFunc={() => openDeleteModal(product)}
                        text="X"
                      />
                    </>
                  }
                />
              ))}
            </ul>
            <div className="cart-total">Total: € {calculateTotal()}</div>
          </div>
        </div>
      ) : (
       <> <p className="cart__empty-msg">{emptyCartMsg}</p>
        <NavLink to="/" className="cart__empty-link">
              Go to shop
            </NavLink>
            </>
      )}

      {modalType === "Delete" && (
        <Modal
          header="Delete?"
          closeButton={true}
          text="Are you sure you want to remove this product from your cart?"
          actions={
            <>
              <Button
                clickFunc={() => removeProductFromCart(activeProduct)}
                text="Yes"
              />
              <Button
                clickFunc={() => dispatch(setModalType("None"))}
                text="No"
              />
            </>
          }
        />
      )}
    </>
  );
};

export { Cart };