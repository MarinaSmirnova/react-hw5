import React from "react";
import PropTypes from "prop-types";

function IconWithInfo({ icon, info, isInfo = true }) {
  return (
    <div className="info-icon">
      {icon}
      {isInfo && <div className="info-icon__circle">{info}</div>}
    </div>
  );
}

IconWithInfo.propTypes = {
  icon: PropTypes.node.isRequired,
  info: PropTypes.number,
  isInfo: PropTypes.bool,
};

IconWithInfo.defaultProps = {
  isInfo: true,
  info: undefined
};

export default IconWithInfo;
